﻿using System;
using System.Collections.Generic;
using System.Text;
using SAP.Middleware.Connector;
using SAPNet;

namespace SAPConnectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Introduzca codigo de compañia:");
            string company = System.Console.ReadLine();
            string name = GetCompanyNamePlus(company);
            System.Console.WriteLine(string.Format("Nombre Compañia: {0}", name));
            System.Console.ReadKey();
        }


 
        /// <summary>
        /// Version del segundo Post http://irevolucion.azurewebsites.net/articulos/sapconnectorplus/
        /// Utiliza las clases helper
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public static string GetCompanyNamePlus(string companyID)
        {

            string companyName = null;

            using (SAPConnection connection = new SAPConnection())
            {
    
                //Obtenemos la funcion
                IRfcFunction companyAPI = connection["BAPI_COMPANY_GETDETAIL"];
                //Fijamos el parametro
                companyAPI.SetValue("COMPANYID", companyID);
                //Invocamos la funcion con la conexion
                companyAPI.Invoke(connection);

                //Obtenemos el result
                SAPResult result = companyAPI.ParseResultFromStructure();
                if (result.Type==SAPTypeResult.Error)
                    throw new Exception(result.Message);

                //Obtenemos la estructura que devuelve la funcion
                IRfcStructure _companyDetail = companyAPI.GetStructure("COMPANY_DETAIL");
                //Obtenemos el campo NAME
                companyName = _companyDetail.GetString("NAME1");

                //Si fuera bapi de grabacion de datos llamariamos a commit
                //connection.Commit();

            }


            return companyName;
        }


        /// <summary>
        /// Version del primer articulo http://irevolucion.azurewebsites.net/articulos/sapconnector/
        /// </summary>
        /// <param name="companyID"></param>
        /// <returns></returns>
        public static string GetCompanyName(string companyID)
        {
            string companyName = null;

                //Obtenemos la conexion definida en el config
                RfcDestination _destinationSAP = RfcDestinationManager.GetDestination("SAP");
                //Inicializamos el contexto de la llamada
                RfcSessionManager.BeginContext(_destinationSAP);
                //Obtenemos la funcion
                IRfcFunction companyAPI = _destinationSAP.Repository.CreateFunction("BAPI_COMPANY_GETDETAIL");
                //Fijamos el parametro
                companyAPI.SetValue("COMPANYID", companyID);
                //Invocamos la funcion
                companyAPI.Invoke(_destinationSAP);
                //Obtenemos la estructura que devuelve la funcion
                IRfcStructure _companyDetail = companyAPI.GetStructure("COMPANY_DETAIL");
                //Obtenemos el campo NAME
                companyName = _companyDetail.GetString("NAME1");
                //Si es null lanzamos excepcion
                if (string.IsNullOrEmpty(companyName)) throw new Exception(string.Format("No se encontro la compañia con codigo {0}", companyID));
                //Finalizamos el contexto de la llamada
                RfcSessionManager.EndContext(_destinationSAP);

       

            return companyName;
        }
    }
}
