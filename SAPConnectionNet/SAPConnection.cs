﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAP.Middleware.Connector;

namespace SAPNet
{

    /// <summary>
    /// Representa una conexion de SAP
    /// </summary>
    public class SAPConnection : IDisposable
    {
        private RfcDestination _ecc = null;

        /// <summary>
        /// Contructor, aqui se inicializa el contexto, el destino sera la entrada clasificada como SAP
        /// </summary>
        public SAPConnection()
        {


            RfcSessionManager.BeginContext(Destination);
        }

        /// <summary>
        /// Abre la conexion al destino indicado
        /// </summary>
        /// <param name="destination"></param>
        public SAPConnection(string destination)
        {
                Destination = RfcDestinationManager.GetDestination(destination);
                RfcSessionManager.BeginContext(Destination);

        }

        /// <summary>
        /// Obtiene la interfaz que hace referencia de la funcion 
        /// </summary>
        /// <param name="function"></param>
        /// <returns></returns>
        public IRfcFunction this[string function]
        {
            get
            {
                return Destination.Repository.CreateFunction(function);
            }
        }

        /// <summary>
        /// Actual destination, por defecto la inicializa con la entrada "SAP
        /// </summary>
        public RfcDestination Destination
        {
            get
            {
                if (_ecc == null)
                    _ecc = RfcDestinationManager.GetDestination("SAP");
                return _ecc;
            }
            set { _ecc = value; }
        }


        /// <summary>
        /// Se 'libera' la conexion (el context)
        /// </summary>
        void IDisposable.Dispose()
        {
            if (_ecc != null)
                RfcSessionManager.EndContext(_ecc);
        }

        /// <summary>
        /// Ejecutar la función de commit, para confirmación de cambios
        /// </summary>
        public void Commit()
        {
            IRfcFunction bapi_commit = this["BAPI_TRANSACTION_COMMIT"];
            bapi_commit.SetValue("WAIT", "X");
            bapi_commit.Invoke(Destination);
        }

    }
}
