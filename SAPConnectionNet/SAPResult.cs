﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SAPNet
{

    /// <summary>
    /// Enumeracion para el tipo de resultado
    /// </summary>
    public enum SAPTypeResult
    {
        Error,
        Info,
        Warning,
        Unknown
    }

    /// <summary>
    /// Coleccion de resultados
    /// </summary>
    public class SAPResults : List<SAPResult>
    {
        /// <summary>
        /// Chequea si hay algun resultado con error
        /// </summary>
        /// <returns></returns>
        public bool HasErrorsSAP()
        {
            return (this.FirstOrDefault<SAPResult>(r => r.Type == SAPTypeResult.Error) != null);
        }
    }

    /// <summary>
    /// Representa un resultado de SAP de la estructura BAPIRETURN
    /// </summary>
    public class SAPResult 
    {
        
        public SAPTypeResult Type { get; set; }
        public string Number { get; set; }
        public string Message { get; set; }
        public string MessageV1 { get; set; }
        public string MessageV2 { get; set; }
        public string MessageV3 { get; set; }
        public string MessageV4 { get; set; }


        /// <summary>
        /// Parse el type a su enumerado
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static SAPTypeResult ParseType(string type)
        {
            SAPTypeResult result = SAPTypeResult.Unknown;
            switch (type)
            {
                case "":
                case "I":
                    result = SAPTypeResult.Info;
                    break;
                case "E":
                    result = SAPTypeResult.Error;
                    break;
                case "W":
                case "S":
                    result = SAPTypeResult.Warning;
                    break;
                default:
                    result = SAPTypeResult.Unknown;
                    break;
            }
            return result;
        }
    }
}
