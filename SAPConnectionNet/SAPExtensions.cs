﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAP.Middleware.Connector;

namespace SAPNet
{
    /// <summary>
    /// Clase utilizada para utilidades de extensiones
    /// </summary>
    public static class  SAPExtensions
    {

        /// <summary>
        /// Parsea la tabla de resultados de SAP  ///  BAPIRET2 && BAPIRETURN
        /// </summary>
        /// <param name="tresults"></param>
        /// <returns></returns>
        public static SAPResults ParseResults(this IRfcTable tresults)
        {

            SAPResults results = new SAPResults();

            for (int i = 0; i < tresults.RowCount; i++)
            {
                IRfcStructure tresult = tresults[i];
                results.Add(ParseResult(tresult));

            }


            return results;

        }

        /// <summary>
        /// Parsea un struct de resultados de sap  /// BAPIRET2 && BAPIRETURN
        /// </summary>
        /// <param name="tresult"></param>
        /// <returns></returns>
        public static SAPResult ParseResult(this IRfcStructure tresult)
        {
            SAPResult result = null;

            if (tresult != null)
            {
                string num = null;
                if (tresult.FirstOrDefault<IRfcField>(f => f.Metadata.Name=="CODE")!=null)
                    num=tresult.GetString("CODE");
                else
                    num= tresult.GetString("NUMBER");
                
                result = new SAPResult()
                {
                    Number = num,
                    Type = SAPResult.ParseType(tresult.GetString("TYPE")),
                    Message = tresult.GetString("MESSAGE"),
                    MessageV1= tresult.GetString("MESSAGE_V1"),
                    MessageV2 = tresult.GetString("MESSAGE_V2"),
                    MessageV3 = tresult.GetString("MESSAGE_V3"),
                    MessageV4 = tresult.GetString("MESSAGE_V4")
                };

            }
    
            

            return result;

        }


        /// <summary>
        /// Parse results desde estructura, la estructura debe ser de tipo  BAPIRET2 && BAPIRETURN y el param. nombre RETURN
        /// </summary>
        /// <param name="funtion"></param>
        /// <returns></returns>
        public static SAPResult ParseResultFromStructure(this IRfcFunction funtion)
        {
            return ParseResult(funtion.GetStructure("RETURN"));
        }


        /// <summary>
        /// Parse results from table, la tabla debe ser de tipo  BAPIRET2 && BAPIRETURNy el param. nombre RETURN
        /// </summary>
        /// <param name="funtion"></param>
        /// <returns></returns>
        public static SAPResults ParseResultsFromTable(this IRfcFunction funtion)
        {
            return ParseResults(funtion.GetTable("RETURN"));
        }


        /// <summary>
        /// Invoca la funcion desde la conexion asociada
        /// </summary>
        /// <param name="function"></param>
        /// <param name="connection"></param>
        public static void Invoke(this IRfcFunction function,SAPConnection connection)
        {
            function.Invoke(connection.Destination);
        }
        
    }
}
